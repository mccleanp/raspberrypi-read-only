#!/bin/bash

echo "Updating package index files..."
apt-get update
 
echo "Removing unwanted packages..."
apt-get remove -y --purge triggerhappy logrotate dphys-swapfile fake-hwclock
apt-get -y autoremove --purge

echo "Disabling automatic updates..."
sudo systemctl disable apt-daily-upgrade.timer
sudo systemctl disable apt-daily.timer
 
echo "Installing busybox-syslogd to replace rsyslog..."
apt-get -y --force-yes install busybox-syslogd; dpkg --purge rsyslog

echo "Enabling systemd watchdog..."
sed -i "s/#RuntimeWatchdogSec=0/RuntimeWatchdogSec=20/" /etc/systemd/system.conf
sed -i "s/#ShutdownWatchdogSec=10min/ShutdownWatchdogSec=2min/" /etc/systemd/system.conf

echo "Configuring file systems to be read only"
sed -i "s/$/ ro/" /boot/cmdline.txt
sed -i -E 's/(vfat\s+defaults)/\1,ro/' /etc/fstab
sed -i -E 's/(ext4\s+defaults)/\1,ro/' /etc/fstab
echo "tmpfs /tmp tmpfs nodev,nosuid 0 0" | sudo tee -a /etc/fstab
echo "tmpfs /var/tmp tmpfs nodev,nosuid 0 0" | sudo tee -a /etc/fstab
echo "tmpfs /var/log tmpfs nodev,nosuid 0 0" | sudo tee -a /etc/fstab
echo "tmpfs /var/spool tmpfs nodev,nosuid 0 0" | sudo tee -a /etc/fstab
echo "tmpfs /var/log tmpfs nodev,nosuid 0 0" | sudo tee -a /etc/fstab
sed -i "s/StateDirectory/#StateDirectory/" /lib/systemd/system/systemd-timesyncd.service
rm -rf /var/lib/systemd/timesync
ln -s /tmp /var/lib/systemd/timesync

